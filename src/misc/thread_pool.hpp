#pragma once

#include <atomic>
#include <thread>
#include <vector>

#include "src/misc/custom_include_spdlog.hpp"

namespace domnrest {
template <typename Clazz>
class ThreadPool {
  typedef void (Clazz::*func_to_call_t)(size_t th_idx);

  Clazz* clazz_;
  func_to_call_t func_to_call_;
  const size_t num_threads_;
  const size_t num_calls_;

 public:
  ThreadPool(Clazz* clazz, func_to_call_t func_to_call, const size_t num_threads, const size_t num_calls)
      : clazz_(clazz), func_to_call_(func_to_call), num_threads_(num_threads), num_calls_(num_calls) {}

  void run() {
    std::atomic_size_t task_num = 0;
    std::vector<std::thread> threads;
    threads.reserve(num_threads_);
    for (size_t thread_num = 0; thread_num < num_threads_; ++thread_num) {
      threads.emplace_back([this, &task_num, thread_num]() {
        SPDLOG_DEBUG(" !! Thread {} spawning", thread_num);
        while (true) {
          const size_t my_task_id = task_num++;
          // SPDLOG_DEBUG("Using threadId")
          if (my_task_id >= num_calls_) {
            break;
          }
          SPDLOG_DEBUG("B::Thread {} starting work {}", thread_num, my_task_id);
          (clazz_->*func_to_call_)(my_task_id);
          SPDLOG_DEBUG("E::Thread {} ended work {}", thread_num, my_task_id);
        }
        SPDLOG_DEBUG(" ** Thread {} ending", thread_num);
      });
    }
    for (size_t thread_num = 0; thread_num < num_threads_; ++thread_num) {
      threads[thread_num].join();
    }
  }
};
}  // namespace domnrest
