#pragma once
#include <cpprest/details/basic_types.h>

namespace domnrest::www {
std::pair<bool, std::string> client_call(utility::string_t url);
}
