#pragma once

#include <cpprest/details/basic_types.h>

#include <algorithm>
#include <string>

namespace domnrest::strutil {

// templated version of my_equal so it could work with both char and wchar_t
template <typename charT>
struct my_equal {
  my_equal() {}

  bool operator()(charT ch1, charT ch2) { return std::toupper(ch1) == std::toupper(ch2); }
};

// find substring (case insensitive)
template <typename T>
size_t find_case_insensitive(const T& str1, const T& str2) {
  typename T::const_iterator it =
      std::search(str1.begin(), str1.end(), str2.begin(), str2.end(), my_equal<typename T::value_type>());
  if (it != str1.end())
    return it - str1.begin();
  else
    return -1;  // not found
}

/**
 * For example, from '<title>Something</title>' will get 'Something'.
 * Should also work with tags with parameters (<title whatever="1"> ...)
 *
 * \param content string to search
 * \param tag_name tag name without <> signs. For example, 'body'
 * \return the result (html tag content / inner text)
 */
std::string extract_html_tag_contents(const std::string& content, const std::string& tag_name);
std::string extract_part_of_html_tag_contents(const std::string& content,
                                              const std::string& tag_name,
                                              std::string::size_type size);

inline int awtoi(const utility::char_t* const input_text) {
#ifdef _MSC_VER
  return _wtoi(input_text);
#else
  return atoi(input_text);
#endif
}
template <typename T>
std::basic_string<utility::char_t> to_ustring(T input) {
  if constexpr (std::is_same<utility::char_t, wchar_t>::value) {
    return std::to_wstring(input);
  } else {
    return std::to_string(input);
  }
}
}  // namespace domnrest::strutil