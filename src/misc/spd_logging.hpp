#pragma once

#include <memory>

#include "custom_include_spdlog.hpp"

namespace spdl {
extern std::string log_pattern;
class SpdlogCfgBuilder;

#define BUILDER_MACRO(variable)                  \
  template <typename A_uto>                      \
  SpdlogCfgBuilder& variable(const A_uto& val) { \
    building_obj.variable = val;                 \
    return *this;                                \
  }

struct SpdlogConfig {
  // Example predefined patterns. Reuse .. or come up with your own.
  constexpr static char PATTERN_ALL_DATA[] = "%^%d.%m.%Y %H:%M:%S.%e %n [%l, %s:%#] %! - %v%$";
  // Pattern without leakage (no file line function)
  constexpr static char PATTERN_NO_LEAKS[] = "%^%d.%m.%Y %H:%M:%S.%e %n [%l] - %v%$";

  friend class SpdlogBuilder;
  std::string default_logger_name{"Logger"};
  std::string pattern{PATTERN_ALL_DATA};
  spdlog::level::level_enum level{spdlog::level::info};

  static SpdlogCfgBuilder build();
};

class SpdlogCfgBuilder {
  SpdlogConfig building_obj;

 public:
  BUILDER_MACRO(default_logger_name);
  BUILDER_MACRO(level);
  BUILDER_MACRO(pattern);

  operator SpdlogConfig&&() { return std::move(building_obj); }
};

std::shared_ptr<spdlog::logger> make_new_logger(const char* name);
void spdlog_setup_default();
spdlog::logger* get(const char* name);
void set_level(const char* name, spdlog::level::level_enum new_lvl);
std::shared_ptr<spdlog::logger>& get_as_shared(const char* name);
void spdlog_setup(SpdlogConfig&& cfg = {});
}  // namespace spdl
