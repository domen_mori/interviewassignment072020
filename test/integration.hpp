#pragma once
#include <rapidjson/error/en.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>

#include "schema_text.hpp"
#include "src/misc/consts.hpp"
#include "src/www/generic_client.hpp"

using namespace std::literals::string_literals;

class ParseValidateHelper {
  typedef rapidjson::
      SchemaValidatingReader<rapidjson::kParseDefaultFlags, rapidjson::StringStream, rapidjson::UTF8<char>>
          DSchemaValidatingReader;
  const std::string input_json_;

  rapidjson::Document schema_document_pre_;
  rapidjson::SchemaDocument schema_document_;
  rapidjson::StringStream json_;
  DSchemaValidatingReader reader_;
  rapidjson::Document document_to_populate_;
  const bool success_;

  static rapidjson::Document make_document() {
    rapidjson::Document d;
    d.Parse(domnrest::SCHEMA);
    return d;
  }

  rapidjson::Document populate_validate_json() {
    rapidjson::Document d;
    d.Populate(reader_);
    return d;
  }

 public:
  ParseValidateHelper(std::string json_text)
      : input_json_(std::move(json_text)),
        schema_document_pre_(make_document()),
        schema_document_(schema_document_pre_),
        json_(input_json_.c_str()),
        reader_(json_, schema_document_),
        document_to_populate_(populate_validate_json()),
        success_(reader_.GetParseResult() && reader_.IsValid()) {}

  std::string error_details() const {
    if (!reader_.GetParseResult()) {
      std::ostringstream oss;

      oss << "Problem with JSON:\n" << input_json_ << "\n";

      const auto code = reader_.GetParseResult().Code();
      oss << "Parse failed with code " << code << " :" << rapidjson::GetParseError_En(code)
          << "\n At offset: " << reader_.GetParseResult().Offset();
      rapidjson::StringBuffer sb;
      reader_.GetInvalidDocumentPointer().StringifyUriFragment(sb);
      oss << " Problematic document part: " << sb.GetString();
      if (!reader_.IsValid()) {
        oss << "\n Conflicting schema keyword: " << reader_.GetInvalidSchemaKeyword();
      }
      return oss.str();
    }
    return "All is OK.";
  }

  bool success() const { return success_; }
};

TEST(ValidateJson, ValidateCorrecthardcodedJson) {
  ParseValidateHelper parsed(domnrest::EXAMPLE_JSON);
  ASSERT_TRUE(parsed.success()) << parsed.error_details();
}

TEST(ValidateJson, DoesNotConformToSchemaHasExtraFields) {
  ParseValidateHelper parsed(domnrest::EXAMPLE_BAD_JSON_EXTRA);
  const auto err_str = parsed.error_details();
  ASSERT_FALSE(parsed.success()) << err_str;
  ASSERT_NE(parsed.error_details().find("additionalProperties"), std::string::npos);
  ASSERT_NE(parsed.error_details().find("something_extra"), std::string::npos);
}

TEST(ValidateJson, InvalidSyntaxJson) {
  ParseValidateHelper parsed(domnrest::INVALID_JSON);
  ASSERT_FALSE(parsed.success()) << parsed.error_details();
}

TEST(ValidateJson, MissingSchemaRequiredFields) {
  ParseValidateHelper parsed(domnrest::EXAMPLE_BAD_JSON_MISSING);
  const auto err_str = parsed.error_details();
  ASSERT_FALSE(parsed.success()) << err_str;
  ASSERT_NE(parsed.error_details().find("required"), std::string::npos);
}

TEST(RestApi, Correct) {
  auto [ok, text] =
      domnrest::www::client_call(domnrest::consts::listener::URL_AS_LOCALHOST_FOR_TESTS + utility::string_t(U("4")));
  ASSERT_TRUE(ok) << "Call error. Make sure that server is online. Additional info:\n " + text;
  ParseValidateHelper parsed(text);
  ASSERT_TRUE(parsed.success()) << parsed.error_details();
}

TEST(RestApi, ParameterMissing) {
  auto [ok, text] = domnrest::www::client_call(domnrest::consts::listener::URL_AS_LOCALHOST_FOR_TESTS);
  ASSERT_FALSE(ok) << "Call error. Make sure that server is online. Additional info:\n " + text;
  ASSERT_EQ(text.find(domnrest::consts::listener::REQ_INTEGER_MISSINGA), utility::string_t::npos);
  ASSERT_EQ(text.find('['), utility::string_t::npos);
  ASSERT_EQ(text.find(']'), utility::string_t::npos);
}

TEST(RestApi, ParameterToBig) {
  auto [ok, text] = domnrest::www::client_call(domnrest::consts::listener::URL_AS_LOCALHOST_FOR_TESTS);
  ASSERT_FALSE(ok) << "Call error. Make sure that server is online. Additional info:\n " + text;
  ASSERT_EQ(text.find(domnrest::consts::listener::REQ_INTEGER_MISSINGA), utility::string_t::npos);
  ASSERT_EQ(text.find('['), utility::string_t::npos);
  ASSERT_EQ(text.find(']'), utility::string_t::npos);
}