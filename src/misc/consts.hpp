﻿#pragma once

#include <cpprest/details/basic_types.h>

#define A_LOCALLY_SCOPED_M_CPP_CHAR_CONST(nm, str) constexpr static char nm##A[] = str;
#define U_LOCALLY_SCOPED_M_CPP_CHAR_CONST(nm, str) constexpr static utility::char_t nm[] = U(str);

#define LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(nm, str) \
  A_LOCALLY_SCOPED_M_CPP_CHAR_CONST(nm, str)         \
  U_LOCALLY_SCOPED_M_CPP_CHAR_CONST(nm, str)

namespace domnrest::consts {

#ifdef NDEBUG
A_LOCALLY_SCOPED_M_CPP_CHAR_CONST(B_TYP, "release")
#else
A_LOCALLY_SCOPED_M_CPP_CHAR_CONST(B_TYP, "debug")
#endif

LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(PROTOTYPING_HTTP_RESULT, "Result.html")

namespace listener {
#ifdef _WIN32
// workaround for testing locally on windows. Listening on 0.0.0.0 requires admin privileges or specific configuration.
// Not the best solution - changing port or path needs change on 2 locations, but good enough for demonstration, I
// believe.
LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(LISTEN_URL_WITH_PORT, "http://127.0.0.1:8081/v1")
#else
LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(LISTEN_URL_WITH_PORT, "http://0.0.0.0:8081/v1")
#endif
LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(URL_AS_LOCALHOST_FOR_TESTS, "http://127.0.0.1:8081/v1/")
LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(REST_INT_PARAMETER_NAME, "integer")
LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(REQ_INTEGER_MISSING, "Integer parameter is missing or invalid!")
LOCALLY_SCOPED_M_AWU_CPP_CHAR_CONST(REQ_INTEGER_TO_HIGH, "Integer parameter must be between 1 and 4 (inclusive)!")
}  // namespace listener
}  // namespace domnrest::consts
