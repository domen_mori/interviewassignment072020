**For testing purposes, we compiled with problematic URLs:**
```c++
  inline static const std::array HTTPCALL_URLS{U("https://www.result.si/projekti/"),
                                               U("https://www.result.si/o-nas/"),
                                               U("http://localhost:8081/v1/aa"),
                                               U("https://www.result.si/kariera/"),
                                               U("http://www.wtffff.si/"),
                                               U("https://www.result.si/blog/")
```

_And the resulting (returned) JSON was:_

```json
{
    "successful_calls": 4,
    "failed_calls": 2,
    "success_text": [
        {
            "text": "Projekti | RESULT\n\t<div id=\"page-container\">\n<div id=\"et-boc\" class=\"et-boc\">\n\t\t\t\n\t",
            "url": "https://www.result.si/projekti/"
        },
        {
            "text": "O nas | RESULT\n\t<div id=\"page-container\">\n<div id=\"et-boc\" class=\"et-boc\">\n\t\t\t\n\t",
            "url": "https://www.result.si/o-nas/"
        },
        {
            "text": "Kariera | RESULT\n\t<div id=\"page-container\">\n<div id=\"et-boc\" class=\"et-boc\">\n\t\t\t\n\t",
            "url": "https://www.result.si/kariera/"
        },
        {
            "text": "Blog | RESULT\n\t<div id=\"page-container\">\n<div id=\"et-boc\" class=\"et-boc\">\n\t\t\t\n\t",
            "url": "https://www.result.si/blog/"
        }
    ],
    "failure_info": [
        {
            "err_message": "Call to http://localhost:8081/v1/aa failed with:\n Error 400: Bad Request",
            "url": "http://localhost:8081/v1/aa"
        },
        {
            "err_message": "Unknown error: Error resolving address",
            "url": "http://www.wtffff.si/"
        }
    ]
}
```