#include "server_component.hpp"

#include <cpprest/filestream.h>
#include <cpprest/http_client.h>
#include <cpprest/http_listener.h>

#include <iomanip>
#include <src/misc/consts.hpp>
#include <src/misc/custom_include_spdlog.hpp>
#include <thread>

#include "src/misc/strings_parse.hpp"
#include "src/www/client_component.hpp"

using http_listener = web::http::experimental::listener::http_listener;
using status_codes = web::http::status_codes;
using http_request = web::http::http_request;
using http_response = web::http::http_response;
using status_code = web::http::status_code;
using methods = web::http::methods;

pplx::task<void> respond(const http_request& request, const status_code& status, const web::json::value& response) {
  SPDLOG_DEBUG("Responding with status {}", status);
  http_response httpresponse(status);
  httpresponse.headers().add(U("Access-Control-Allow-Origin"), U("*"));
  httpresponse.set_body(response);
  return request.reply(httpresponse);
}

void domnrest::www::ServerComponent::listen_and_block() {
  web::http::experimental::listener::http_listener_config config;
  http_listener listener(consts::listener::LISTEN_URL_WITH_PORT);

  try {
    listener.open().wait();
  } catch (const std::exception& ex) {
    SPDLOG_CRITICAL("All died. Exception: {}", ex.what());
    return;
  }

  listener.support(methods::GET, [](const http_request& req) {
    const auto& got_uri = req.request_uri();
    const auto& p1 = got_uri.path();

    const int parameter_value = domnrest::strutil::awtoi(&*(std::find(std::rbegin(p1), std::rend(p1), '/') - 1));

    if (parameter_value <= 0) {
      SPDLOG_INFO(consts::listener::REQ_INTEGER_MISSINGA);
      respond(req,
              status_codes::BadRequest,
              web::json::value::array({web::json::value::string(consts::listener::REQ_INTEGER_MISSING)}))
          .wait();
      return;
    }
    if (parameter_value > 4) {
      SPDLOG_INFO(consts::listener::REQ_INTEGER_TO_HIGHA);
      respond(req,
              status_codes::BadRequest,
              web::json::value::array({web::json::value::string(consts::listener::REQ_INTEGER_TO_HIGH)}))
          .wait();
      return;
    }

    SPDLOG_INFO("Received request: {}", parameter_value);

    ClientComponent component(parameter_value);
    component.call();

    respond(req, status_codes::OK, component.get_job_result()).wait();
  });
  SPDLOG_INFO(
      "{} build; Waiting for incoming connection... ({})", consts::B_TYPA, consts::listener::LISTEN_URL_WITH_PORTA);
  // just wait forever.
  while (true) {
    std::this_thread::sleep_for(std::chrono::seconds(16));
  }
}
