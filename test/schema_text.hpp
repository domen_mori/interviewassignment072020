#pragma once

namespace domnrest {

constexpr char EXAMPLE_BAD_JSON_EXTRA[] =
    R"({
   "successful_calls":3,
   "failed_calls":1,
   "something_extra":42,
   "success_text":[]
})";

constexpr char EXAMPLE_BAD_JSON_MISSING[] =
    R"({
   "successful_calls":3,
   "success_text":[]
})";

constexpr char INVALID_JSON[] =
    R"({
   "successful_calls":3,
   "success_text": whateverBadstr
})";

constexpr char EXAMPLE_JSON[] =
    "{\n"
    "   \"successful_calls\":3,\n"
    "   \"failed_calls\":1,\n"
    "   \"success_text\":[\n"
    "      {\n"
    "         \"text\":\"t1\",\n"
    "         \"url\":\"https://www.result.si/projekti/\"\n"
    "      },\n"
    "      {\n"
    "         \"text\":\"oNas\",\n"
    "         \"url\":\"https://www.result.si/o-nas/\"\n"
    "      },\n"
    "      {\n"
    "         \"text\":\"Blog\",\n"
    "         \"url\":\"https://www.result.si/blog/\"\n"
    "      }\n"
    "   ],\n"
    "   \"failure_info\":[\n"
    "      {\n"
    "         \"err_message\":\"Call to https://www.result.si/karieraq/ failed with:\\n Error 404: Not Found\",\n"
    "         \"url\":\"https://www.result.si/karieraq/\"\n"
    "      }\n"
    "   ]\n"
    "}";

constexpr char SCHEMA[] =
    R"special({
  "type": "object",
  "properties": {
    "successful_calls": {
      "type": "integer",
      "minimum": 0,
      "maximum": 4
    },
    "failed_calls": {
      "type": "integer",
      "minimum": 0,
      "maximum": 4
    },
    "success_text": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "text": {
            "type": "string",
            "description": "Title and first symbols of body (in practice this is usually a bunch of html tags)"
          },
          "url": {
            "type": "string",
            "description": "Url of successfull call"
          }
        },
        "required": [
          "text",
          "url"
        ],
        "additionalProperties": false
      }
    },
    "failure_info": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "url": {
            "type": "string",
            "description": "Url of unsuccessfull call"
          },
          "err_message": {
            "type": "string",
            "description": "Error code (if known) and short error info."
          }
        },
        "required": [
          "url",
          "err_message"
        ],
        "additionalProperties": false
      }
    }
  },
  "required": [
    "successful_calls",
    "failed_calls"
  ],
  "additionalProperties": false
})special";
}  // namespace domnrest
