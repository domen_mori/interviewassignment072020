#pragma once

namespace domnrest::www {
class ServerComponent {
 public:
  static void listen_and_block();
};
}  // namespace domnrest::www
