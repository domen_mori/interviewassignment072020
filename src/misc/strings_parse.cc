#include "strings_parse.hpp"

void error_check(std::string::size_type str_found_idx) {
  if (str_found_idx == std::string::npos) {
    throw std::runtime_error("bad HTML input.");
  }
}

/*
Personal notes:

The HTML spec from WHATWG indicates the opening bracket must be immediately followed by the tag name.
The XML spec from W3C has the same requirements.

You don't have to worry about space leading the tag name. However, there can be any amount of space between the end of
the tag name and the closing bracket. The following is valid:

<p           ></p         >
<p
></p>

End tags must have the following format:

The first character of an end tag must be a U+003C LESS-THAN SIGN character (<).
The second character of an end tag must be a U+002F SOLIDUS character (/).
The next few characters of an end tag must be the element's tag name.
After the tag name, there may be one or more ASCII whitespace.
Finally, end tags must be closed by a U+003E GREATER-THAN SIGN character (>).

 */

/**
 * \param content
 * \param tag_name
 * \param less_than_sign
 *
 * \return first: index of '<'. Second: index of '>'.
 */
std::pair<std::string::size_type, std::string::size_type> find_html_tag_idx(const std::string& content,
                                                                            const std::string& tag_name,
                                                                            const char* const less_than_sign) {
  auto left_idx = domnrest::strutil::find_case_insensitive(content, less_than_sign + tag_name);
  error_check(left_idx);
  std::string::size_type right_idx = content.find('>', left_idx);
  error_check(right_idx);
  return std::make_pair(left_idx, right_idx);
}

std::string domnrest::strutil::extract_html_tag_contents(const std::string& content, const std::string& tag_name) {
  const auto content_start = find_html_tag_idx(content, tag_name, "<").second + 1;
  const auto content_end = find_html_tag_idx(content, tag_name, "</").first;
  return content.substr(content_start, content_end - content_start);
}

std::string domnrest::strutil::extract_part_of_html_tag_contents(const std::string& content,
                                                                 const std::string& tag_name,
                                                                 std::string::size_type size) {
  const auto content_start = find_html_tag_idx(content, tag_name, "<").second + 1;
  return content.substr(content_start, size);
}
