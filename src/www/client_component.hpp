#pragma once

#include <cpprest/details/basic_types.h>
#include <cpprest/json.h>

#include <array>

namespace domnrest::www {

class ClientComponent {
  inline static const std::array HTTPCALL_URLS{U("https://www.result.si/projekti/"),
                                               U("https://www.result.si/o-nas/"),
                                               U("https://www.result.si/kariera/"),
                                               U("https://www.result.si/blog/")};

  const int parallel_strength_;

  std::array<std::string, HTTPCALL_URLS.size()> results_{};
  std::array<bool, HTTPCALL_URLS.size()> success_{};
  void job(size_t th_idx);
  static std::string extract_some_content(const std::string& html);
  std::pair<web::json::value, web::json::value> create_scrapedtext_array() const;

 public:
  explicit ClientComponent(const int parallel_strength);

  void call();
  [[nodiscard]] web::json::value get_job_result() const;
};
}  // namespace domnrest::www
