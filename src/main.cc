#include "misc/spd_logging.hpp"
#include "www/server_component.hpp"

int main(int argc, char** argv) {
  spdl::spdlog_setup(spdl::SpdlogConfig::build()
                         .default_logger_name("ResultRestClient")
                         .level(spdlog::level::debug)
                         .pattern(spdl::SpdlogConfig::PATTERN_ALL_DATA));
  domnrest::www::ServerComponent::listen_and_block();
  return 0;
}
