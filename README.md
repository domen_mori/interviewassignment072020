#### Povzetek
Poženemo na enega izmed načinov:
* "blocking" način - v terminalu prikazana konzola programa

```
docker run -p 3002:3002 -p 8081:8081 domenn/result-rest-072020:release2
```

* Interaktivni način. Odpre se bash, program (endpoint) je potrebno pognati posebej (glejte spodaj - poti).

```
docker run -it --entrypoint /bin/bash -p 3002:3002 -p 8081:8081  domenn/result-rest-072020:release2
```

##### Poti
* "release" program: `/project/my_src/cmake-build-prod/ResultRestClient`
* "debug" program (več izpisov v konzolo): `/project/my_src/cmake-build-dbg/ResultRestClient`
* **URL** (api): <http://localhost:8081/v1/4>
    * številka na koncu (4) je parameter
* **URL** (swagger): <http://localhost:3002/?displayRequestDuration=true&url=http://localhost:3002/swagger.yaml>
    * parametri za `?` so seveda opcijski. `http://localhost:3002/swagger.yaml` lahko vpišemo tudi v vrstico swaggerjevega vmesnika in kliknemo "Explore"
* Testi (vsi trije so globalno dosegjivi - v _path_):
    * "unit tests": `drestapi-unittestsnonetwork.sh`
        * neodvisni samostojni testi brez zunanjih odvisnosti ("dependencies")
    * "integration (api) tests": `drestapi-integrationtests.sh`
        * testirajo API, potrebujejo pognan glavni program in omrežni dostop do njega. Gre za točko 4 iz zahtev - preverjanje skladnosti APIja s specifikacijo.
    * obojni oz. vsi: `ResultRestClient_tests`
        * datoteka se nahaja tudi v `/project/my_src/cmake-build-prod` in `/project/my_src/cmake-build-dbg`
* Swagger - datoteke: `/project/swagger_build/swagger-ui-3.30.2` ukaz `npm start`
    * poženemo ga lahko tudi s `powerup_swagger.sh` (globalno, non-blocking)
        * S tem se požene v screen instanci, poimenovani `ScreenSwagger` (vidno z ukazom `screen -list`)
* V `/project/my_src/` se nahaja izvorna koda, ki je tudi git repozitorij.
        
`localhost` po potrebi seveda zamenjamo z dejanskim naslovom strežnika.

#### Podrobna razlaga
Postopki opisano v nadaljevanju so bolj splošne narave in ljudem s tehnično podlago verjetno dobro znani, zato branje
verjetno ni potrebno.

Pri zagonu brez `--entrypoint <string>` (opisano čisto na vrhu - blocking način) in naknadni uporabi <kbd>Ctrl</kbd>+<kbd>C</kbd>
se v mojem okolju (Docker for Windows, Powershell) program ne ustavi. Ustavim ga z `docker stop`.


Za zagon integracijskih testov mora biti program zagnan. Če imamo konzolo blokirano zaradi izpisa programa lahko
seveda uporabimo drugo konzolo (`docker exec -it <hash/name>` /bin/bash). Če želimo vse opraviti z 
eno konzolo, lahko npr. poženemo container 
s parametrom `--entrypoint /bin/bash` in ročno poženemo program (in opcijsko swagger) preko ene izmed zgoraj omenjenih poti
(lahko uporabimo `&>/dev/null &`, s tem sicer izgubimo izpis v konzolo, ali pa ga poženemo
npr. v screen instanci).

* `screen -S <ime> -dm <comand>` požene `<command>` v screen instanci v ozadju.
* `screen -R <ime>` attacha screen, da vidimo izpis programa.
* <kbd>Ctrl</kbd>+<kbd>D</kbd> detacha screen, da lahko ponovno tipkamo v konzolo.

Seveda si lahko vedno pomagamo s `ps aux`, da preverimo obstoječe instance swaggerja, našega programa in screena.

URLji so "hardcoded" v programu. Windows verzija posluša le na 127.0.0.1, linux, ki se izvaja tudi na dockerju, pa 0.0.0.0 torej povsod, tudi preko
interneta. Integracijski testi (za API) uporabljajo `localhost`, torej morajo biti
pognani na isti napravi (program lokalno v dockerju / WSL in testi neposredno na docker-host računalniku bodo najverjetneje delovali). 