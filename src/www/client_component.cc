#include "client_component.hpp"

#include <src/misc/custom_include_spdlog.hpp>

#include "generic_client.hpp"
#include "src/misc/strings_parse.hpp"
#include "src/misc/thread_pool.hpp"

using namespace std::literals::string_literals;

domnrest::www::ClientComponent::ClientComponent(const int parallel_strength) : parallel_strength_(parallel_strength) {}

void domnrest::www::ClientComponent::call() {
  ThreadPool<domnrest::www::ClientComponent> pool(
      this, &ClientComponent::job, parallel_strength_, HTTPCALL_URLS.size());
  pool.run();
}

void domnrest::www::ClientComponent::job(decltype(HTTPCALL_URLS)::size_type th_idx) {
  auto [ok, result_text] = www::client_call(HTTPCALL_URLS[th_idx]);
  if (ok) {
    try {
      std::tie(success_[th_idx], results_[th_idx]) = std::make_pair(true, extract_some_content(result_text));
      SPDLOG_TRACE("Result  {}", results_[th_idx]);
    } catch (const std::exception& ex) {
      SPDLOG_ERROR(ex.what());
      success_[th_idx] = false;
      results_[th_idx] = "Unable to parse. Perhaps bad html. Additional info: "s + ex.what();
    }
  } else {
    results_[th_idx] = std::move(result_text);
  }
}

std::string domnrest::www::ClientComponent::extract_some_content(const std::string& html) {
  const auto title_content = domnrest::strutil::extract_html_tag_contents(html, "title");

  const auto some_body = domnrest::strutil::extract_part_of_html_tag_contents(html, "body", 66);

  return title_content + some_body;
}

std::pair<web::json::value, web::json::value> domnrest::www::ClientComponent::create_scrapedtext_array() const {
  constexpr auto KEYURL = U("url");
  constexpr auto KEYTXT = U("text");
  constexpr auto KEYERR = U("err_message");

  web::json::value good;
  web::json::value bad;
  for (size_t i = 0; i < results_.size(); ++i) {
    const auto [currently_writing, key_txt] =
        success_[i] ? std::make_pair(&good, KEYTXT) : std::make_pair(&bad, KEYERR);
    currently_writing->operator[](currently_writing->size())[KEYURL] = web::json::value::string(HTTPCALL_URLS[i]);
    currently_writing->operator[](currently_writing->size() - 1)[key_txt] =
        web::json::value::string(utility::conversions::to_string_t(results_[i]));
  }
  return std::make_pair(good, bad);
}

web::json::value domnrest::www::ClientComponent::get_job_result() const {
  web::json::value json_value(web::json::value::object(true));
  const auto ok = std::count(success_.begin(), success_.end(), true);
  json_value[U("successful_calls")] = web::json::value::number(ok);
  json_value[U("failed_calls")] = web::json::value::number(success_.size() - ok);
  auto [json_ok, json_failure] = create_scrapedtext_array();
  if (json_ok.size() > 0) {
    json_value[U("success_text")] = std::move(json_ok);
  }
  if (json_failure.size() > 0) {
    json_value[U("failure_info")] = std::move(json_failure);
  }
  return json_value;
}
