#include "spd_logging.hpp"

#include <map>
#include <memory>
#include <src/misc/custom_include_spdlog.hpp>
#include <vector>

bool log_to_file{true};
spdlog::level::level_enum my_level{spdlog::level::level_enum::info};

std::map<std::string, std::shared_ptr<spdlog::logger>> all_loggers;
std::string spdl::log_pattern;

void spdl::spdlog_setup_default() {
  constexpr const char DEFAULT_LOGGER_NAME[] = "Spdlogs";
  spdlog::set_default_logger(get_as_shared(DEFAULT_LOGGER_NAME));
  SPDLOG_DEBUG("Test logging as debug.");
}
std::shared_ptr<spdlog::logger> spdl::make_new_logger(const char *name) {
  bool file_ok{true};
  std::vector<spdlog::sink_ptr> sinks;
  sinks.reserve(3);
#ifdef _WIN32
#ifndef NDEBUG
  sinks.emplace_back(std::make_shared<spdlog::sinks::msvc_sink_mt>());
  spdlog::sinks::msvc_sink_mt *vs_sink = static_cast<spdlog::sinks::msvc_sink_mt *>(sinks.back().get());
#endif
#endif
  // sinks.emplace_back(internal::ColorfulSinkFactorySingleton::make_colorful_stdout());
  sinks.emplace_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
  for (auto &sink : sinks) {
    sink->set_level(my_level);
  }
  sinks.shrink_to_fit();
  auto shartedptr_lg = std::make_shared<spdlog::logger>(name, sinks.begin(), sinks.end());
  shartedptr_lg->set_level(my_level);

  shartedptr_lg->set_pattern(log_pattern);
#ifdef _WIN32
#ifndef NDEBUG
  vs_sink->set_pattern("%g(%#): " + log_pattern);
#endif
#endif
  return shartedptr_lg;
}
std::shared_ptr<spdlog::logger> &spdl::get_as_shared(const char *name) {
  if (auto iterator = all_loggers.find(name); iterator != all_loggers.end()) {
    return iterator->second;
  }
  auto created = all_loggers.emplace(name, make_new_logger(name));
  return created.first->second;
}
spdlog::logger *spdl::get(const char *name) { return get_as_shared(name).get(); }
void spdl::spdlog_setup(SpdlogConfig &&cfg) {
  log_pattern = std::move(cfg.pattern);

  my_level = cfg.level;

  if (!cfg.default_logger_name.empty()) {
    spdlog::set_default_logger(get_as_shared(cfg.default_logger_name.c_str()));
  }
}
void spdl::set_level(const char *name, spdlog::level::level_enum new_lvl) {
  spdl::get(name)->set_level(new_lvl);
  std::for_each(spdl::get(name)->sinks().begin(), spdl::get(name)->sinks().end(), [new_lvl](auto &itm) {
    itm->set_level(new_lvl);
  });
}

spdl::SpdlogCfgBuilder spdl::SpdlogConfig::build() { return {}; }