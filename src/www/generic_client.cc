#include "generic_client.hpp"

#include <cpprest/http_client.h>

using http_response = web::http::http_response;
using status_codes = web::http::status_codes;
using status_code = web::http::status_code;
using methods = web::http::methods;

using namespace std::literals::string_literals;

std::pair<bool, std::string> domnrest::www::client_call(utility::string_t url) {
  pplx::task<std::pair<bool, std::string>> the_task = pplx::create_task([&url] {
                                                        web::http::client::http_client client(url);
                                                        return client.request(methods::GET);
                                                      }).then([&url](http_response response) {
    if (response.status_code() == status_codes::OK) {
      const auto extract_task = response.extract_utf8string();
      extract_task.wait();
      return std::make_pair(true, extract_task.get());
    }
    return std::make_pair(false,
                          "Call to " + utility::conversions::to_utf8string(url) + " failed with:\n Error " +
                              std::to_string(response.status_code()) + ": " +
                              utility::conversions::to_utf8string(response.reason_phrase()));
  });
  try {
    the_task.wait();
    return the_task.get();
  } catch (const std::exception& e) {
    return std::make_pair(false, "Unknown error: "s + e.what());
  }
}
