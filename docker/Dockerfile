FROM ubuntu:20.04
RUN apt update

ADD docker/dev_create/install_tzdata.sh /install_tzdata.sh
RUN /install_tzdata.sh

RUN apt install -y ssh \
    build-essential \
    gcc \
    g++ \
    gdb \
    clang \
    cmake \
    rsync \
    tar \
    python libspdlog-dev libcpprest-dev libgtest-dev rapidjson-dev git && apt-get clean

RUN mkdir -p /project/my_src/cmake-build-prod && mkdir -p /project/plibs/gtest && cd  /project/plibs/gtest &&  cmake /usr/src/gtest && make

ADD .git /project/my_src/.git
RUN cd /project/my_src && git reset --hard HEAD && cd cmake-build-prod && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j 6 \
&& mkdir /project/my_src/cmake-build-dbg && cd /project/my_src/cmake-build-dbg && cmake -DCMAKE_BUILD_TYPE=Debug .. && make -j 6

RUN apt install -y nodejs npm screen && mkdir /project/swagger_build && cd /project/swagger_build \
&& wget https://github.com/swagger-api/swagger-ui/archive/v3.30.2.tar.gz \
&& tar xzf v3.30.2.tar.gz \
&& cd swagger-ui-3.30.2 \
&& npm install && npm run build \
&& cp /project/my_src/swagger/swagger.yaml dist

RUN cp /project/my_src/cmake-build-prod/ResultRestClient_tests /bin \
&&  cp /project/my_src/docker/drestapi-integrationtests.sh /bin \
&& cp /project/my_src/docker/drestapi-unittestsnonetwork.sh /bin \
&& cp /project/my_src/docker/powerup_blocking.sh /bin \
&& cp /project/my_src/docker/powerup_swagger.sh /bin \
&& chmod +x /bin/drestapi-unittestsnonetwork.sh \
&& chmod +x /bin/drestapi-integrationtests.sh \
&& chmod +x /bin/powerup_swagger.sh \
&& chmod +x /bin/powerup_blocking.sh

EXPOSE 8081 3002
CMD ["powerup_blocking.sh"]
ENTRYPOINT ["powerup_blocking.sh"]