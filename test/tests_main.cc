#include <gtest/gtest.h>

#include <src/misc/strings_parse.hpp>
#include <test/integration.hpp>

class HtmlParseFixtureParam {
 public:
  const std::string input_{};
  const std::string tag_{};
  const std::string expected_{};
};

class HtmlParseFixture : public ::testing::TestWithParam<HtmlParseFixtureParam> {};

TEST_P(HtmlParseFixture, CorrectSyntaxParses) {
  const auto [input, tag, expected] = GetParam();

  ASSERT_EQ(expected, domnrest::strutil::extract_html_tag_contents(input, tag));
}

TEST(HtmlParsing, BadHtmlInput) {
  ASSERT_ANY_THROW(domnrest::strutil::extract_html_tag_contents("<body><h1>body</h1></b>", "body"));
}

INSTANTIATE_TEST_SUITE_P(HtmlParseTests,
                         HtmlParseFixture,
                         ::testing::Values(HtmlParseFixtureParam{"<body>body</body>", "body", "body"},
                                           HtmlParseFixtureParam{"<body><h1>body</h1></body>", "h1", "body"},
                                           HtmlParseFixtureParam{"<BODY>aBc</BODY>", "body", "aBc"},
                                           HtmlParseFixtureParam{"<title a=\"b\">aBc</title>", "title", "aBc"},
                                           HtmlParseFixtureParam{"<body extra_items=123>  < h1 >body< /h1 > </body   >",
                                                                 "body",
                                                                 "  < h1 >body< /h1 > "}));

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
