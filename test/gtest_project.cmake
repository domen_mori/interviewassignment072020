

file(GLOB_RECURSE ResultRestClient_tests_glob_src LIST_DIRECTORIES false "${CMAKE_CURRENT_SOURCE_DIR}/test/*.cc"
    "${CMAKE_CURRENT_SOURCE_DIR}/test/*.hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/test/*.h"
    )

add_executable(ResultRestClient_tests ${ResultRestClient_tests_glob_src}
    src/misc/strings_parse.cc src/misc/strings_parse.hpp src/www/generic_client.cc src/www/generic_client.hpp)

find_package(RapidJSON CONFIG REQUIRED)

if (WIN32)
  target_link_libraries(ResultRestClient_tests PRIVATE GTest::gtest)
  target_include_directories(ResultRestClient_tests PRIVATE ${RAPIDJSON_INCLUDE_DIRS})
else ()
  target_link_libraries(ResultRestClient_tests PRIVATE ${GTEST_LIBRARIES})
endif ()
target_link_libraries(ResultRestClient_tests PRIVATE spdlog::spdlog cpprestsdk::cpprest)